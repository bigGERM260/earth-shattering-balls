import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Ball extends Ellipse2D implements Runnable{
	//gui variables
	private GamePanel gamePanel;
	public Explosion explosion = null;
	private Color color;
	
	//size and position variables
	private Point currentPoint;
	private Point earthCenter;
	private Point startingPoint;
	private Dimension dimension;
	private final Ellipse2D.Double circumference;
	private double randRadius;
	private int xShift = 1, yShift = 1;
	
	//game flow variables
	private final long TIMER_SPEED = 50;
	private double speed;
	private Boolean running = true;
	private char character = '0';
	
	
	public Ball(GamePanel gamePanel, double speed, Dimension dimension, Point earthCenter, Ellipse2D.Double circumference){
		//initialize dimension d
		this.gamePanel = gamePanel;
		this.speed = speed;
		this.dimension = dimension;
		this.earthCenter = earthCenter;
		this.circumference = circumference;
		
		//generate number to randomly select color
		generateColor();
		//generate attack side
		generateAttackPoint();
		//generate the size of the ball
		generateSize();
		//determine ball traversal path
		determinePath();
	}
	
	@Override
	public void run() {
		while(running){
			try {
				//delay the next iteration
				Thread.sleep(TIMER_SPEED);
			} catch (InterruptedException e) {
				
			}
			//move the ball toward earth
			moveBall();
			checkOutOfBounds();
			gamePanel.repaint();
		}
	}

	//move the ball 
	public void moveBall(){
		//update current ball position
		currentPoint.x += speed*xShift;
		currentPoint.y += speed*yShift;
	}
	
	//calculates the path of the ball when created, once to improve process speed
	//instead of performing each of these calculations in each call to move, removes redundancy
	public void determinePath(){
		double xRand = Math.random();
		double yRand = Math.random();
		
		//determine x movement
		if(startingPoint.x <= earthCenter.x){ //ball started to the left of earth
			if(xRand < 0.22 && startingPoint.x > 200 && startingPoint.x < 500){
				xShift = 0;
			}
			else if(xRand < 0.66){
				xShift = 2;
			}
			else { //xRand < 1.0
				xShift = 1;
			}
		}
		else{ //ball started to the right of earth 
			if(xRand < 0.22 && startingPoint.y > 200 && startingPoint.y< 500 ){
				xShift = 0;
			}
			else if(xRand < 0.66){
				xShift = -2;
			}
			else { //xRand < 1.0
				xShift = -1;
			}
		}
		
		//determine y movement
		if(startingPoint.y <= earthCenter.y){ //ball started above earth
			if(yRand < 0.22 && xShift != 0 && startingPoint.x > 120 && startingPoint.x < 480 ){
				yShift = 0;
			}
			else if(yRand < 0.66){
				yShift = 2;
			}
			else {
				yShift = 1;
			}
		}
		else { //ball started below earth
			if(yRand < 0.22 && xShift != 0 && startingPoint.x > 120 && startingPoint.x< 480 ){
				yShift = 0;
			}
			else if(yRand < 0.66){
				yShift = -2;
			}
			else {
				yShift = -1;
			}
		}
	}
	
	//check if this ball is out of the jpanel or hit earth
	public void checkOutOfBounds(){
		//check if ball is off of the game screen, if so, delete to preserve memory
		if(circumference.contains(currentPoint) || currentPoint.x < -40 || currentPoint.x > 740 || currentPoint.y < -40 || currentPoint.y > 640){
			//display explosion if hit earth
			if(circumference.contains(currentPoint)){
				try {
					explosion = new Explosion(currentPoint, gamePanel);
					int index = gamePanel.determineIndex(character);
					if(index != -1){
						try{
							//set the specific ball in the array of arraylist, in gamePanel, to zero stop drawing it
							gamePanel.ballHandler[index].get(gamePanel.ballHandler[index].indexOf(this)).randRadius = 0;
							
							//check if this same balls' explosion size is greater than 100, which means the explosion has grown to
							//its max and the ball is no longer needed as a reference to draw the explosion in the paintComponent 
							//class, so ball can be deleted freeing removing all references to ball and explosion (for garbage collector)
							if(gamePanel.ballHandler[index].get(gamePanel.ballHandler[index].indexOf(this)).explosion.getSize() > 100){
								gamePanel.ballHandler[index].remove(gamePanel.ballHandler[index].indexOf(this));
							}
							
							//update gamePanel score board
							gamePanel.updateScore(-10);
							
							//increment the number of hits earth has taken
							gamePanel.hitEarth();
						} catch(Exception e){
							//System.out.println(e +" Index: "+index);
						}
					}
					//gamePanel.explosions.add(explosion);
				} catch (IOException e) {}
				
			}
			//remove ball from arraylist, and stop the while loop in the run()
			//gamePanel.determine
			//gamePanel.balls.remove(this);
			
			running = false;
		}
	}
	//generate the size of the ball
	public void generateSize(){
		//generate random number to arbitrarily determine the size
		randRadius = (Math.random()*30)+10; //generate random radius between 10 and 30
	}
	
	//generate the attack point
	public void generateAttackPoint(){
		//generate random number to arbitrarily determine the side the attack comes from
		double randSide = Math.random();
		
		if(randSide < .25){ //start from left side of screen
			currentPoint = new Point(1, generateCoordinate('x'));
		}
		else if(randSide < .5){ //start from right side of screen
			currentPoint = new Point(660, generateCoordinate('x'));
		}
		else if(randSide < .75){ //start from top of the screen
			currentPoint = new Point(generateCoordinate('y'), 1);
		}
		else if(randSide <= 1.0){ //start from bottom of the screen
			currentPoint = new Point(generateCoordinate('y'), 540);
		}
		startingPoint = new Point((int) currentPoint.getX(), (int) currentPoint.getY());
	}
	
	//generate the starting pixel position based on the dimension of the window
	//and whether the Point needs a x coordinate or a y coordinate
	public int generateCoordinate(char xORy){
		//local variables
		double coordinate = 0;

		//get coodinate based on char parameter xORy
		switch(xORy){
		case 'x':
			coordinate = Math.random()*dimension.getWidth();
			//System.out.println("X Coordinate: "+coordinate);
			break;
		case 'y':
			coordinate = Math.random()*dimension.getHeight();
			//System.out.println("Y Coordinate: "+coordinate);
			break;
		default:
			System.out.println("Error Generating Coordinate!");
		}
		return (int) coordinate;
	}
	
	//generate random number to arbitrarily pick color of ball and kill key
	public void generateColor(){
		double randColor = Math.random();
		if(randColor < .12){
			color = new Color(100,7,139);
			character = 'v';
		}
		else if(randColor < .24){
			color = new Color(248, 114, 3);
			character = 'o';
		}
		else if(randColor < .36){
			color = Color.yellow;
			character = 'y';
		}
		else if(randColor < .48){
			color = Color.green;
			character = 'g';
		}
		else if(randColor < .60){
			color = Color.blue;
			character = 'b';
		}
		else if(randColor < .72){
			color = new Color(255, 100, 175);
			character = 'p';
		}
		else if(randColor < .84){
			color = Color.white;
			character = 'w';
		}
		else { //if < 1.0
			color = Color.red;	
			character = 'r';
		}
	}
	
	public char getChar(){
		return character;
	}
	
	public Color getColor(){
		return color;
	}

	@Override
	public Rectangle2D getBounds2D() {
		return null;
	}
	
	@Override
	public double getHeight() {
		return randRadius;
	}
	
	@Override
	public double getWidth() {
		return randRadius;
	}

	@Override
	public double getX() {
		return currentPoint.x;
	}

	@Override
	public double getY() {
		return currentPoint.y;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void setFrame(double arg0, double arg1, double arg2, double arg3) {
		
	}
}
