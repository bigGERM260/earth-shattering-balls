import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

public class Explosion{
	//class variables
	private Image img;
	private Point collisionPoint;
	private GamePanel gamePanel;
	private double tempX, tempY;
	private double size;
	
	//class constructor
	public Explosion(Point collisionPoint, GamePanel gamePanel) throws IOException{
		//initialize class variables
		size = 50;
		this.collisionPoint = collisionPoint;
		this.gamePanel = gamePanel;
		img = ImageIO.read(new File("explosion1.png"));
		tempX = collisionPoint.getX();
		tempY = collisionPoint.getY();
	}
	
	//get image
	public Image getImage(){
		return img;
	}
	
	//set image
	public void setImage(){
		img = null;
	}
	
	//get x coordinate
	public int getX(){
		return collisionPoint.x;
	}
	
	//get y coordinate
	public int getY(){
		return collisionPoint.y;
	}
	
	//get the size of the image
	public double getSize(){
		return size;
	}
	
	//set size
	public void setSize(double adder){
		//adjust size of the explosion gradually(called in paintcomponent
		size += adder;
		//as size increases  coordinates need to be modified
		tempX -= adder/3;
		tempY -= adder/3;
		collisionPoint.x = (int)tempX;
		collisionPoint.y = (int)tempY;
		if(size > 100){
			img = null;
		}
		//gamePanel.explosions.remove(this); //this causes a ConcurrentModificationException
	}
}
