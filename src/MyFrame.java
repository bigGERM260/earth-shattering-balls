import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MyFrame extends JFrame {
	//class variables
	MenuPanel menuPanel;
	
	public MyFrame(){
		//menu features
		JMenuBar mbar = new JMenuBar();
		JMenu mGame = new JMenu("Game");
		JMenu mView = new JMenu("View");
		JMenuItem iMain = new JMenuItem("New Game");
		JMenuItem iPause = new JMenuItem("Pause/Resume");
		JMenuItem iInstructions = new JMenuItem("Instructions");
		JMenuItem iHScore = new JMenuItem("High Score");
		JMenuItem iExit = new JMenuItem("Exit");
		iExit.addActionListener(new exitListener());
		mGame.add(iMain);
		mGame.add(iPause);
		mGame.add(iInstructions);
		mGame.add(iExit);
		mView.add(iHScore);
		mbar.add(mGame);
		mbar.add(mView);
		setJMenuBar(mbar);
		
		//display main menu panel
		menuPanel = new MenuPanel(this);
		
		//finally set panel to frame and display
		add(BorderLayout.CENTER, menuPanel);
		setMinimumSize(new Dimension(700,600));
		setResizable(false);
		setVisible(true);
	}
	
	//this class listens for a button press to begin the game
	public class displayMenuListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			add(BorderLayout.CENTER, menuPanel);
			
			menuPanel.setVisible(true);
		}
	}
	
	//this class listens for when the new game menu item is pressed
	public class newGameListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//menuPanel.setVisible(true);
		}
	}

	//this classlistens for when the exit menu is pressed
	public class exitListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.exit(0);
		}
		
	}
}
