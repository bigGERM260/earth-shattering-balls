import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	//gui variables
	private Graphics2D g2;
	private MyFrame frame;
	private GamePanel gamePanel;
	public JLabel lScore;
	private Color color;
	private Image img = null;
	private Image gameOverImg = null;
	
	//position and size variables
	private Dimension dimension;
	private final Point earthCenter;
	private Point startingPoint;
	private int randRadius;
	private Ellipse2D.Double circumference;
	
	//constants
	//private final int ballRadius = 20;
	//private final int earthRadius = 250;
	private final int CIRCUM_RADIUS = 240;
	
	//game flow variables
	@SuppressWarnings("unchecked")
	public ArrayList<Ball>[] ballHandler = (ArrayList<Ball>[]) new ArrayList[8];
	private int difficulty;
	private double speed;
	private final int MAX_HITS = 25;
	public int score = 0;
	private int hits = 0;
	private int ballCount = 0;
	private long delay;
	private int addBallRate = 1;
	private int marker;
	
	public GamePanel(MyFrame frame, int difficulty){
		//initialize variables
		this.frame = frame;
		this.difficulty = difficulty;
		gamePanel = this;
		dimension = new Dimension(frame.getWidth(), frame.getHeight());
		earthCenter = new Point(((int)frame.getWidth()/2)-15,((int)frame.getHeight()/2)-7);
		
		//initialize array, of empty arraylist
		for(int i=0; i<8; i++)
			ballHandler[i] = new ArrayList<Ball>();
		
		//set speed of ball movement and delay between creating each new ball
		if(difficulty == 3){
			delay = 400;
			speed = 1.3;
		}
		else if(difficulty == 2){
			delay = 500;
			speed = 1.0;
		}
		else{ //difficulty == 1 
			delay = 600;
			speed = .7;
		}
		
		//score label, display game score
		lScore = new JLabel("Score: "+score);
		lScore.setFont(new Font("Arial", Font.BOLD, 20));
		lScore.setForeground(Color.red);
		gamePanel.add(lScore);
		
		//create earth circumference, used for determining collisions with earth
		circumference = new Ellipse2D.Double(earthCenter.x-(CIRCUM_RADIUS/2), earthCenter.y-(CIRCUM_RADIUS/2)-5, CIRCUM_RADIUS, CIRCUM_RADIUS);		
		
		//get the background image
		try{		
			img = ImageIO.read(new File("earthAdjusted.jpg"));
		}
		catch(IOException e){
			System.out.println("ERROR: Image not found.");
		}
		
		//set keystroke listener
		this.setFocusable(true);
		this.addKeyListener(new KeyStroke());
		
		//set layout
		setLayout(new FlowLayout(10,20,20));
		repaint();
		
		//establish timer, and set delay time between each creation of a new ball
		Timer timer = new Timer();
		timer.schedule(new NewBall(), 0, delay);
	}
	
	//paint component
	public void paintComponent(Graphics g) throws ConcurrentModificationException {
		super.paintComponent(g);
		g2 = (Graphics2D) g;
		
		//draw the background image
		g2.drawImage(img, 0,0,700,600, null);
				
		//draw all balls in ballHandler
		if(ballHandler != null){
			for(ArrayList<Ball> list: ballHandler){
				for(Ball b: list){
					g2.setColor(b.getColor());
					g2.fill(b);
					if(b.explosion != null){
						g2.drawImage(b.explosion.getImage(), b.explosion.getX(), b.explosion.getY(), (int)b.explosion.getSize(), (int)b.explosion.getSize(), null);
						b.explosion.setSize(0.2);
					}
				}
			}
		}
		//draw the game over explosion
		/*if(gameOverImg != null){
			g2.drawImage(gameOverImg, earthCenter.x, earthCenter.y, )
		}*/
		
		//draw circumference
		g2.setColor(new Color(0,0,0,0));
		g2.fill(circumference);
	}
	
	//method determines which sub array the letter belongs to
	public int determineIndex(char character){
		int index = -1;
		switch(character){
		case 'r':
			index = 0;
			break;
		case 'p':
			index = 1;
			break;
		case 'o':
			index = 2;
			break;
		case 'y':
			index = 3;
			break;
		case 'g':
			index = 4;
			break;
		case 'b':
			index = 5;
			break;
		case 'v':
			index = 6;
			break;
		case 'm':		//let 'm' and 'v' be purple (violet, magenta)
			index = 6;
			break;
		case 'w':
			index = 7;
			break;
		}
		return index;
	}
	
	//find the closest ball to earth to remove
	public int findClosestBall(int index){
		int closest = 0;
		double min = 400;
		double x=0, y=0;
		double distance = 0;
		for(int i=0; i<ballHandler[index].size(); i++){
			//calculate distance from Earth to ball
			x = Math.abs(ballHandler[index].get(i).getX() - earthCenter.getX());
			y = Math.abs(ballHandler[index].get(i).getY() - earthCenter.getY());
			//if the ball in the sub arraylist is closer to earth than min
			distance = Math.sqrt((x*x) + (y*y));
			if(distance < min){
				min = distance;
				closest = i;
			}
		}
		return closest;
	}
	
	//updates the score of the game 
	public void updateScore(int adder){
		score += adder;
		lScore.setText("Score: "+score);
		repaint();
	}
	
	//updates the number of hits earth has taken and checks if earth is dead
	public void hitEarth(){
		hits++;
		//earth hit too many times game over.
		if(hits == MAX_HITS){
			System.out.println("Game Over!");
			try {
				//delete all balls
				for(int i=0; i<ballHandler.length; i++){
					ballHandler[i]=new ArrayList<Ball>();
				}
				//set array to null
				ballHandler = null;
				img = ImageIO.read(new File("explosion2."));
			} 
			catch (IOException e) {
		
			}
		}
	}
	
	//create a new ball and add to the proper sub array
	public void addBall(){
		//create new ball
		Ball tempBall = new Ball(gamePanel, speed, dimension, earthCenter, circumference);
		//start new thread for moving the ball
		Thread tempThread = new Thread(tempBall);
		tempThread.start();
		
		//determine the correct sub array to add the new ball to
		int index = determineIndex(tempBall.getChar());
		if(index != -1 && ballHandler != null){
			ballHandler[index].add(tempBall);
		}	
		//increment ball count
		ballCount++;
		
		//monitor ballCount
		if(ballCount > marker){
			//increment number of balls created every x seconds
			addBallRate++;
			//reset ballCount
			ballCount = 0;
			//raise the point when addBallRate is increased
			marker = (int)(marker*1.3);
		}
	}
	
	public void removeBall(){
		
	}
			
	//key stroke listener
	class KeyStroke implements KeyListener {
		
		@Override
		public void keyPressed(KeyEvent key) {	
			int index = determineIndex(key.getKeyChar());
			if(index != -1 && !ballHandler[index].isEmpty()){
				//pressed a key that is both valid and the color appears on the screen
				//remove the closest ball to earth of that color
				ballHandler[index].remove(gamePanel.findClosestBall(index));
				//add points to the score 
				updateScore(20);
			}
			else{
				//pressed a key that is invalid or color doesn't exist, and is penalized 
				//add a new ball to score for invalid key pressed
				addBall();
				//take points off the score
				updateScore(-10);
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}

	//class extends timer task, this is needed to avoid multiple inheritance as java doesn't support it
	class NewBall extends TimerTask {
		
		//class constructor
		public NewBall(){
			run();
		}
		
		//timer task to be executed every x number of milliseconds
		public void run(){
			//for(int i=0; i<addBallRate; i++){
				addBall();
			//}
		}		
	}
}
