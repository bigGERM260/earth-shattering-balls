import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class MenuPanel extends JPanel{
	//class variable
	private Graphics2D g2;
	private String fileName;
	private MyFrame frame;
	private MenuPanel menuPanel = this;
	private int bWidth = 180;
	private int bHeight = 60;
	
	public MenuPanel(MyFrame frame){
		//initialize frame
		this.frame = frame;
		
		//set image file name
		fileName = "shower1.png";
		
		//set panel layout
		setLayout(new FlowLayout(10,20,30));	
		this.setMaximumSize(new Dimension(700,600));
		
		//main menu buttons
		//easy button properties
		JButton bEasy = new JButton("Easy");
		bEasy.setPreferredSize(new Dimension(bWidth, bHeight));
		bEasy.setFont(new Font("Arial", Font.BOLD, 30));
		bEasy.setBackground(new Color(20,20,20));
		bEasy.setForeground(Color.ORANGE);
		bEasy.addActionListener(new displayGameListener());
		//medium button properties
		JButton bMedium = new JButton("Medium");
		bMedium.setPreferredSize(new Dimension(bWidth, bHeight));
		bMedium.setFont(new Font("Arial", Font.BOLD, 30));
		bMedium.setBackground(new Color(20,20,20));
		bMedium.setForeground(new Color(248, 114, 3));
		bMedium.addActionListener(new displayGameListener());
		//hard button properties
		JButton bHard = new JButton("Hard");
		bHard.setPreferredSize(new Dimension(bWidth, bHeight));
		bHard.setFont(new Font("Arial", Font.BOLD, 30));
		bHard.setBackground(new Color(20,20,20));
		bHard.setForeground(Color.red);
		bHard.addActionListener(new displayGameListener());
	
		//set jlabel title
		JLabel lTitle = new JLabel(" Earth Shattering");
		JLabel lTitle2 = new JLabel("       Balls       ");
		lTitle.setForeground(Color.orange);
		lTitle.setFont(new Font("Arial", Font.BOLD+Font.ITALIC, 65));
		lTitle2.setForeground(Color.orange);
		lTitle2.setFont(new Font("Arial", Font.BOLD+Font.ITALIC, 85));
		add(lTitle);
		add(lTitle2);
	
		//add buttons in proper position
		int count = 0;
		JButton[] butts = {bEasy, bMedium, bHard};
			
		for(int i=0; i<3; ++i){	
			add(new JLabel("                                                    "));
			add(butts[count]);
			add(new JLabel("                                                    "));
			count++;
		}
	}
	

	//paint component
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g2 = (Graphics2D) g;
		
		Image img = null;
		try{		
			img = ImageIO.read(new File(fileName));
		}
		catch(IOException e){
			System.out.println("ERROR: Image not found.");
		}
		
		//create background
		g2.drawImage(img, 0,0,700,600, null);
	}
	
	//this class listens for a button press to begin the game
	public class displayGameListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//determine which button was clicked for the difficulty
			String str = e.getActionCommand();
			int difficulty;
			if(str.equals("Hard")){
				difficulty = 3;
			}
			else if(str.equals("Medium")){
				difficulty = 2;
			}
			else{
				difficulty = 1;
			}
			
			//create game panel and display
			GamePanel gamePanel = new GamePanel(frame, difficulty);
			frame.add(gamePanel);
			//hide menu panel
			menuPanel.setVisible(false);
			//repaint my frame
			frame.setVisible(true);
		}
	}
}
